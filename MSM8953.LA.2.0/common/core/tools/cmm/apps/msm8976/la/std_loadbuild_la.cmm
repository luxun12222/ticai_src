//============================================================================
//  Name:                                                                     
//    std_loadbuild_la.cmm 
//
//  Description:                                                              
//    LA Specific Build loading script
//                                                                            
// Copyright (c) 2012 - 2014 by Qualcomm Technologies, Incorporated.  All Rights Reserved.        
//
//
//
//
//                      EDIT HISTORY FOR FILE
//  This section contains comments describing changes made to the module.
//  Notice that changes are listed in reverse chronological order.
//
// when       who     		what, where, why
// --------   ---     		---------------------------------------------------------
// 09/29/2015 c_gunnan		Added devcfg.mbn binary flashing support
// 08/26/2015 c_gunnan      Added keymaster and cmnlib binary flashing support
// 07/13/2015 c_gunnan      Created for 8976
// 10/01/2012 AJCheriyan	Removed SDI image loading for good (needs boot support)
// 09/13/2012 AJCheriyan	Added SDI image loading (add partition.xml changes)
// 09/07/2012 AJCheriyan	Removed SDI image loading (revert partition.xml changes )
// 09/02/2012 AJCheriyan	Added SDI image loading
// 08/08/2012 AJCheriyan	Fixed issue with paths for mjsdload
// 07/19/2012 AJCheriyan    Created for B-family 

// ARG0 - Load option - Supported : ERASEONLY, LOADCOMMON, LOADFULL
ENTRY &ARG0 &ARG1

LOCAL &CWD &SEARCHPATHS &STORAGE_OPTION &STORAGE_TYPE &PROGRAMMER &PARTITION
LOCAL &XML &MAX_PARTITIONS &XML_LOCATION &BINARY &BINARYPATH &METASEARCHPATH

&PROGRAMMER="boot_images/core/storage/tools/jsdcc/mjsdload"
&MAX_PARTITIONS=1
&XML_LOCATION="&METASCRIPTSDIR/../../../build"
&PARTITION=0

MAIN:
	// We have checked for all the intercom sessions at this point and we don't need any error
	// Save the argument
	&LOAD_OPTION="&ARG0"

	// Switch to the tools directory
	&CWD=OS.PWD()
	
	cd &CWD
	
	&SEARCHPATHS="&XML_LOCATION"
	
	// Erase only
	IF (("&ARG0"=="ERASEONLY")||("&ARG0"=="LOADCOMMON")||("&ARG0"=="LOADFULL"))
	(
		// Only erase the chip and exit
		CD.DO &BOOT_BUILDROOT/&PROGRAMMER ERASE
	)

	// Load common images
	IF (("&ARG0"=="LOADCOMMON")||("&ARG0"=="LOADFULL"))
	(
		// Check for all the common images 

		// Check for the presence of all the binaries
		// Not needed because meta-build should have populated all this information
		// SBL, TZ, RPM, APPSBL, SDI
		do std_utils FILEXIST FATALEXIT &BOOT_BUILDROOT/&BOOT_BINARY
		do std_utils FILEXIST FATALEXIT &RPM_BUILDROOT/&RPM_BINARY
		do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&APPSBOOT_BINARY
		do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&QSEE_BINARY
		//do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&QHEE_BINARY

		;WAIT 5s
	
		// Now flash them all one by one 
		// Flash the partition table
		&PARTITION=0
		WHILE (&PARTITION<&MAX_PARTITIONS)
        (
            &XML="rawprogram"+FORMAT.DECIMAL(1, &PARTITION)+".xml"
            &FILES="gpt_main"+FORMAT.DECIMAL(1, &PARTITION)+".bin,"+"gpt_backup"+FORMAT.DECIMAL(1,&PARTITION)+".bin"
            CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=&XML files=&FILES
            &PARTITION=&PARTITION+1
        )
	
		// Flash sbl/tz/hyp/rpm,emmc_appsboot
         &SEARCHPATHS="&XML_LOCATION,"+OS.FILE.PATH(&BOOT_BUILDROOT/&BOOT_BINARY)+","+OS.FILE.PATH(&TZ_BUILDROOT/&QSEE_BINARY)+","+OS.FILE.PATH(&RPM_BUILDROOT/&RPM_BINARY)+","+OS.FILE.PATH(&APPS_BUILDROOT/&APPSBOOT_BINARY)
	
	
		  // Apply the disk patches
        &PARTITION=0
        WHILE (&PARTITION<&MAX_PARTITIONS)
        (
            &XML="patch"+FORMAT.DECIMAL(1, &PARTITION)+".xml"
            CD.DO &BOOT_BUILDROOT/&PROGRAMMER PATCH searchpaths=&SEARCHPATHS xml=&XML
            &PARTITION=&PARTITION+1
        )
			
		&PARTITION=0
        WHILE (&PARTITION<&MAX_PARTITIONS)
        (
            &XML="rawprogram"+FORMAT.DECIMAL(1, &PARTITION)+".xml"
            CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=&XML files=sbl1.mbn,tz.mbn,hyp.mbn,devcfg.mbn,rpm.mbn,emmc_appsboot.mbn,keymaster.mbn,cmnlib.mbn
		    &PARTITION=&PARTITION+1
        )
		

	)
	    // Load common images
    IF ("&ARG0"=="LOADIMG")
    (
        // Check for the binary first 
        IF ("&ARG1"=="sbl")
        (
            do std_utils FILEXIST FATALEXIT &BOOT_BUILDROOT/&BOOT_BINARY
           
            &BINARY="sbl1.mbn"
			&BINARYPATH=OS.FILE.PATH("&BOOT_BUILDROOT/&BOOT_BINARY")    
        )
        IF ("&ARG1"=="tz")
        (
          	do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&QSEE_BINARY
			//do std_utils FILEXIST FATALEXIT &TZ_BUILDROOT/&QHEE_BINARY
            &BINARY="tz.mbn,hyp.mbn,devcfg.mbn"
            &BINARYPATH=OS.FILE.PATH("&TZ_BUILDROOT/&QSEE_BINARY")
        )
        IF ("&ARG1"=="rpm")
        (
            do std_utils FILEXIST FATALEXIT &RPM_BUILDROOT/&RPM_BINARY
            &BINARY="rpm.mbn"
            &BINARYPATH=OS.FILE.PATH("&RPM_BUILDROOT/&RPM_BINARY")
        )
        
        IF ("&ARG1"=="appsboot")
        (
            do std_utils FILEXIST FATALEXIT &APPS_BUILDROOT/&APPSBOOT_BINARY
            &BINARY="emmc_appsboot.mbn"
            &BINARYPATH=OS.FILE.PATH("&APPS_BUILDROOT/&APPSBOOT_BINARY")
        )

        // Flash the image now
        &SEARCHPATHS="&XML_LOCATION,"+"&BINARYPATH"
        &PARTITION=0
        WHILE (&PARTITION<&MAX_PARTITIONS)
        (
            &XML="rawprogram"+FORMAT.DECIMAL(1, &PARTITION)+".xml"
            CD.DO &BOOT_BUILDROOT/&PROGRAMMER LOAD searchpaths=&SEARCHPATHS xml=&XML files=&BINARY
            &PARTITION=&PARTITION+1
        )
    )
      
        
    
	// Load HLOS images
	IF ("&LOAD_OPTION"=="LOADFULL")
	(
		// Change the active partition. This is needed only if the user flashes an HLOS that needs
		// a different partition
		CD.DO &BOOT_BUILDROOT/&PROGRAMMER 9 activeboot=0

   		// Call the script to fastboot the remaining images
		OS.COMMAND cmd /k python &METASCRIPTSDIR/../../../build/fastboot_all.py --ap=&APPS_BUILDROOT --pf=&PRODUCT_FLAVOR
		
	)

	// Return to the old directory
	CD &CWD

	GOTO EXIT


FATALEXIT:
	END

EXIT:
	ENDDO