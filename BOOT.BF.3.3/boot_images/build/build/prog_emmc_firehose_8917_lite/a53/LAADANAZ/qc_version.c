/*
#============================================================================
#  Name:                                                                     
#    qc_version.c 
#
#  Description:                                                              
#    None 
#                                                                            
# Copyright (c) 2017 by QUALCOMM, Incorporated.  All Rights Reserved.        
#============================================================================
#                                                                            
# *** AUTO GENERATED FILE - DO NOT EDIT                                      
#                                                                            
# GENERATED: Wed Jun 21 02:06:04 2017 
#============================================================================
*/
const char QC_IMAGE_VERSION_STRING_AUTO_UPDATED[]="QC_IMAGE_VERSION_STRING=BOOT.BF.3.3-00221";
