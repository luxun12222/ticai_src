Logging to /local/mnt/workspace/CRMBuilds/CNSS.PR.4.0-00397-M8953BAAAANAZW-1_20170622_004347/b/wcnss_proc/Pronto/bsp/devcfgimg/build/arm9/SCAQMAZ/reloc/sign/SecImage_log.txt
Config path is set to: /local/mnt/workspace/CRMBuilds/CNSS.PR.4.0-00397-M8953BAAAANAZW-1_20170622_004347/b/wcnss_proc/tools/build/scons/sectools/config/integration/secimage.xml
Output dir is set to: /local/mnt/workspace/CRMBuilds/CNSS.PR.4.0-00397-M8953BAAAANAZW-1_20170622_004347/b/wcnss_proc/Pronto/bsp/devcfgimg/build/arm9/SCAQMAZ/reloc/sign
------------------------------------------------------
Processing 1/1: /local/mnt/workspace/CRMBuilds/CNSS.PR.4.0-00397-M8953BAAAANAZW-1_20170622_004347/b/wcnss_proc/build/ms/bin/SCAQMAZ/reloc/wcnss.mbn

WARNING: Loadable segment - 4 is of size: 0
WARNING: Loadable segment - 6 is of size: 0
WARNING: Loadable segment - 8 is of size: 0
WARNING: Loadable segment - 9 is of size: 0
Signing image: /local/mnt/workspace/CRMBuilds/CNSS.PR.4.0-00397-M8953BAAAANAZW-1_20170622_004347/b/wcnss_proc/build/ms/bin/SCAQMAZ/reloc/wcnss.mbn
Generating new Root certificate and a random key
Generating new Attestation CA certificate and a random key
Generating new Attestation certificate and a random key
Signed image is stored at /local/mnt/workspace/CRMBuilds/CNSS.PR.4.0-00397-M8953BAAAANAZW-1_20170622_004347/b/wcnss_proc/Pronto/bsp/devcfgimg/build/arm9/SCAQMAZ/reloc/sign/default/wcnss/wcnss.mbn

Base Properties: 
| Integrity Check             | True  |
| Signed                      | True  |
| Encrypted                   | False |
| Size of signature           | 256   |
| Size of one cert            | 2048  |
| Num of certs in cert chain  | 3     |
| Number of root certs        | 1     |
| Cert chain size             | 6144  |

ELF Properties: 
Elf Header: 
| Magic                      | ELF                           |
| Class                      | ELF32                          |
| Data                       | 2's complement, little endian  |
| Version                    | 1 (Current)                    |
| OS/ABI                     | No extensions or unspecified   |
| ABI Version                | 0                              |
| Type                       | EXEC (Executable file)         |
| Machine                    | Advanced RISC Machines ARM     |
| Version                    | 0x1                            |
| Entry address              | 0x8da018ac                     |
| Program headers offset     | 0x00000034                     |
| Section headers offset     | 0x00000000                     |
| Flags                      | 0x05000002                     |
| ELF header size            | 52                             |
| Program headers size       | 32                             |
| Number of program headers  | 11                             |
| Section headers size       | 40                             |
| Number of section headers  | 0                              |
| String table section index | 0                              |

Elf Program Headers: 
| S.No | Type | Offset | VirtAddr | PhysAddr | FileSize | MemSize |   Flags    |   Align   |
|------|------|--------|----------|----------|----------|---------|------------|-----------|
|  1   | LOAD |0x003000|0x8da00000|0x8da00000| 0x00332c | 0x003460| 0x88000007 | 0x100000  |
|  2   | LOAD |0x00afcc|0x8da04000|0x8da04000| 0x000000 | 0x009000| 0x08000006 | 0x4000    |
|  3   | LOAD |0x00afcc|0x8da0d000|0x8da0d000| 0x00f000 | 0x00f000| 0x08000006 | 0x4       |
|  4   | LOAD |0x019fcc|0x8da1c000|0x8da1c000| 0x000000 | 0x00e000| 0x08000006 | 0x4       |
|  5   | LOAD |0x019fcc|0x8da2a000|0x8da2a000| 0x343670 | 0x50f240| 0x08000007 | 0x80      |
|  6   | LOAD |0x35d63c|0x8df39280|0x8df39280| 0x000000 | 0x0411b8| 0x08000006 | 0x8       |
|  7   | LOAD |0x35d63c|0x8df39280|0x8df39280| 0x000000 | 0x021c44| 0x08000006 | 0x4       |
|  8   | LOAD |0x35d63c|0x8df80000|0x8df80000| 0x000038 | 0x0001b8| 0x08000006 | 0x8       |
|  9   | LOAD |0x35efcc|0x8df81000|0x8df81000| 0x00a304 | 0x00df80| 0x08000007 | 0x1000    |
|  10  | LOAD |0x3692d0|0x8df8f000|0x8df8f000| 0x08c000 | 0x08c000| 0x08000006 | 0x4       |
|  11  | LOAD |0x3f5fcc|0x8e01b000|0x8e01b000| 0x01c29c | 0x01c29c| 0x08000006 | 0x1000    |

Hash Segment Properties: 
| Header Size  | 40B  |
| Has Preamble | False|
| Preamble Size| None |
| Has Magic Num| False|
| Page Size    | None |
| Num Pages    | None |
| Ota Enabled  | False|
| Ota Min Size | None |

Header: 
| cert_chain_ptr  | 0x8e0382c8  |
| cert_chain_size | 0x00001800  |
| code_size       | 0x000001a0  |
| flash_parti_ver | 0x00000003  |
| image_dest_ptr  | 0x8e038028  |
| image_id        | 0x00000000  |
| image_size      | 0x00001aa0  |
| image_src       | 0x00000000  |
| sig_ptr         | 0x8e0381c8  |
| sig_size        | 0x00000100  |

------------------------------------------------------

SUMMARY:
Following actions were performed: "sign"
Output is saved at: /local/mnt/workspace/CRMBuilds/CNSS.PR.4.0-00397-M8953BAAAANAZW-1_20170622_004347/b/wcnss_proc/Pronto/bsp/devcfgimg/build/arm9/SCAQMAZ/reloc/sign

| Idx | SignId | Parse | Integrity | Sign | Encrypt |              Validate              |
|     |        |       |           |      |         | Parse | Integrity | Sign | Encrypt |
|-----|--------|-------|-----------|------|---------|-------|-----------|------|---------|
|  1. | wcnss  |   T   |     NA    |  T   |    NA   |   NA  |     NA    |  NA  |    NA   |

