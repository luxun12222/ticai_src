#include <debug.h>
#include <platform/gpio.h>
#include <i2c_qup.h>
#include <blsp_qup.h>
#include <platform/irqs.h>
#include <kernel/thread.h>
#include <display_resource.h>
#include <qtimer.h>
#include <err.h>


/*******************************************************

   1、LT8911B的IIC地址：
   a)如果LT8911B的第31脚（S_ADR）为低，则LT8911B的I2C 地址为0x52; // bit0 是读写标志位；如果是Linux系统，IIC address 的 bit7作为读写标志位，则I2C_Adr 应该是 0x29

   b)如果LT8911B的第31脚（S_ADR）为高，则LT8911B的I2C 地址为0x5a; // bit0 是读写标志位；如果是Linux系统，IIC address 的 bit7作为读写标志位，则I2C_Adr 应该是 0x2d

   2、IIC速率不要超过100KHz。

   3、要确定MIPI信号给到LT8911B之后，再初始化LT8911B。

   4、必须由前端主控GPIO来复位LT8911B；刷寄存器之前，先Reset LT8911B ,用GPIO 先拉低LT8911B的复位脚 100ms左右，再拉高，保持100ms。


   5、LT8911B 对MIPI输入信号的要求：
   a) MIPI DSI
   b) Video mode
   c) non-burst mode（continue mode）--（MIPI 的CLK是要连续的）
   d) sync event

 *********************************************************/

//#define _Test_Pattern_ // 输出黑白竖条的test pattern
#define _1080P_eDP_Panel_

// 设置输入的MIPI信号的Lane数
#define _MIPI_Lane_ 4                                   // MIPI Lane 1,2,3,4

#ifdef _1080P_eDP_Panel_
// 根据前端MIPI信号的Timing，修改以下参数：
static int MIPI_Timing[] =
    //H_act V_act	 H_total V_total H_BP	 H_sync  V_sync  V_BP
    {1920,1080,2080,1111,80,32,5,16};

// 根据eDP屏的规格书，定义LT8911B eDP输出的Lane数。
#define _2_Lane_
// 根据eDP屏的色深，定义LT8911B 的色深设置
#define _8_Bit_ColorDepth_ // eDP panel Color Depth，16.7M color
#endif

enum
{
	H_act = 0,
	V_act,
	H_tol,
	V_tol,
	H_bp,
	H_sync,
	V_sync,
	V_bp
};

unsigned char swing_req = 0x00;

#define Swing_Level_Close 0x00

#define Swing_Level_0_H 0x00
#define Swing_Level_0_L 0xa0

#define Swing_Level_1_H 0x00
#define Swing_Level_1_L 0xf0

#define Swing_Level_2_H 0x01
#define Swing_Level_2_L 0x40

#define Swing_Level_3_H 0x02
#define Swing_Level_3_L 0xb4

static unsigned char	DPCD0000H;
static unsigned char	DPCD0001H;
static unsigned char	DPCD0002H;
static unsigned char	DPCD0003H;
static unsigned char	DPCD0004H;
static unsigned char	DPCD0005H;
static unsigned char	DPCD0006H;
static unsigned char	DPCD0007H;
static unsigned char	DPCD0008H;
static unsigned char	DPCD0009H;
static unsigned char	DPCD000aH;
static unsigned char	DPCD000bH;
static unsigned char	DPCD0200H;
static unsigned char	DPCD0201H;
unsigned char			DPCD0202H;
static unsigned char	DPCD0203H;
static unsigned char	DPCD0204H;
unsigned char			DPCD0205H;
unsigned char			DPCD0206H;

unsigned char			Count	   = 0x00;
unsigned char			Cur_Temp   = 0x00;
unsigned char			Pre_Temp   = 0x00;
unsigned char			D_value	   = 0x00;

static struct qup_i2c_dev *i2c_dev;
static int LK_lt8911_i2c_init()
{
	i2c_dev = qup_blsp_i2c_init(BLSP_ID_1,QUP_ID_2,100000,19200000);
	if(!i2c_dev) {
		dprintf(CRITICAL, "mipi_dsi_i2c_device_init() failed\n");
		return ERR_NOT_VALID;
	}
	return NO_ERROR;
}
static unsigned char lt8911b_i2c_read(unsigned char reg_addr)
{
    int ret = 0;
    unsigned char val = 0 ;
    struct i2c_msg msg_buf[] = {
        {0x29, I2C_M_WR, 1, &reg_addr},
        {0x29, I2C_M_RD, 1, &val}
    };
    ret = qup_i2c_xfer(i2c_dev, msg_buf, 2);
    if(ret < 0) {
        dprintf(CRITICAL, "qup_i2c_xfer error %d\n", ret);
        return ret;
    }
    return val;
}
static int lt8911b_i2c_write(unsigned char reg_addr, unsigned char val)
{
    int ret = 0;
    unsigned char data_buf[] = { reg_addr, val };
    struct i2c_msg msg_buf[] = {
               {0x29, I2C_M_WR, 2, data_buf}
    };
    ret = qup_i2c_xfer(i2c_dev, msg_buf, 1);
    if(ret < 0) {
        dprintf(CRITICAL, "qup_i2c_xfer error %d\n", ret);
        return ret;
    }
    return 0;
}



void DpcdWrite(unsigned int Address, unsigned char Data)
{
	unsigned char	AddressH   = 0x0f & (Address >> 16);
	unsigned char	AddressM   = 0xff & (Address >> 8);
	unsigned char	AddressL   = 0xff & Address;

	lt8911b_i2c_write(0xff, 0x80);

	lt8911b_i2c_write(0x62, 0xbd);
	lt8911b_i2c_write(0x62, 0xbf);   // ECO(AUX reset)

	lt8911b_i2c_write(0x36, 0x00);
	lt8911b_i2c_write(0x30, 0x0f);   // 0x0f , 0x10
	lt8911b_i2c_write(0x33, AddressL);
	lt8911b_i2c_write(0x34, AddressM);
	lt8911b_i2c_write(0x35, AddressH);
	lt8911b_i2c_write(0x37, Data);
	lt8911b_i2c_write(0x36, 0x20);
}


unsigned char DpcdRead(unsigned int Address)
{
	unsigned char	read_cnt   = 0x03;
	unsigned char	DpcdValue  = 0x00;
	unsigned char	AddressH   = 0x0f & (Address >> 16);
	unsigned char	AddressM   = 0xff & (Address >> 8);
	unsigned char	AddressL   = 0xff & Address;

	lt8911b_i2c_write(0xff, 0x80);

	lt8911b_i2c_write(0x62, 0xbd);
	lt8911b_i2c_write(0x62, 0xbf);   // ECO(AUX reset)

	lt8911b_i2c_write(0x36, 0x00);
	lt8911b_i2c_write(0x30, 0x8f);   //0x8f , 0x90
	lt8911b_i2c_write(0x33, AddressL);
	lt8911b_i2c_write(0x34, AddressM);
	lt8911b_i2c_write(0x35, AddressH);
	lt8911b_i2c_write(0x36, 0x20);

	mdelay(2);                      //必要的

	if(lt8911b_i2c_read(0x39) == 0x01)
	{
		DpcdValue = lt8911b_i2c_read(0x38);
	//	printf("LHT DpcdValue1[%02x]\n",DpcdValue);
	}else
	{
		while((lt8911b_i2c_read(0x39) != 0x01) && (read_cnt > 0))
		{
			lt8911b_i2c_write(0x36, 0x00);
			lt8911b_i2c_write(0x36, 0x20);
			read_cnt--;
			mdelay(2);
		}

		if(lt8911b_i2c_read(0x39) == 0x01)
		{
			DpcdValue = lt8911b_i2c_read(0x38);
			//printf("LHT DpcdValue2[%02x]\n",DpcdValue);
		}
	}
	return DpcdValue;
}


void adj_swing(void)
{
	unsigned char ret = 0;

	swing_req = DPCD0206H & 0x0f;   //lane 0
	lt8911b_i2c_write(0xff, 0x81);

	switch(swing_req)
	{
		case 0x00:                  //0dB_400mV
			lt8911b_i2c_write(0x18, 0x00);
			lt8911b_i2c_write(0x19, 0xa0);
			lt8911b_i2c_write(0x11, 0x00);
			ret = 0x00;
			break;

		case 0x01:                  //0dB_600mV
			lt8911b_i2c_write(0x18, 0x00);
			lt8911b_i2c_write(0x19, 0xd0);
			                        //lt8911b_i2c_write(0x10,0x00);
			lt8911b_i2c_write(0x11, 0x00);
			ret = 0x01;
			break;

		case 0x02:                  //0dB_800mV
			lt8911b_i2c_write(0x18, 0x01);
			lt8911b_i2c_write(0x19, 0x20);
			                        //lt8911b_i2c_write(0x10,0x00);
			lt8911b_i2c_write(0x11, 0x00);
			ret = 0x02;
			break;

		case 0x03:                  //0dB_1200mV(max 1000mV)
			lt8911b_i2c_write(0x18, 0x01);
			lt8911b_i2c_write(0x19, 0xa0);
			                        //lt8911b_i2c_write(0x10,0x00);
			lt8911b_i2c_write(0x11, 0x00);
			ret = 0x07;
			break;

		case 0x04:                  //3.5dB_400mV
			lt8911b_i2c_write(0x18, 0x00);
			lt8911b_i2c_write(0x19, 0x98);
			                        //lt8911b_i2c_write(0x10,0x00);
			lt8911b_i2c_write(0x11, 0x28);
			ret = 0x08;
			break;

		case 0x05:                  //3.5dB_600mV
			lt8911b_i2c_write(0x18, 0x00);
			lt8911b_i2c_write(0x19, 0xf0);
			                        //lt8911b_i2c_write(0x10,0x00);
			lt8911b_i2c_write(0x11, 0x38);
			ret = 0x09;
			break;

		case 0x06:                  //3.5dB_800mV
			lt8911b_i2c_write(0x18, 0x01);
			lt8911b_i2c_write(0x19, 0xa0);
			                        //lt8911b_i2c_write(0x10,0x00);
			lt8911b_i2c_write(0x11, 0x70);
			ret = 0x0a;
			break;

		case 0x07:
			break;

		case 0x08:  //6dB_400mV
			lt8911b_i2c_write(0x18, 0x00);
			lt8911b_i2c_write(0x19, 0xa0);
			        //lt8911b_i2c_write(0x10,0x00);
			lt8911b_i2c_write(0x11, 0x44);
			ret = 0x10;
			break;

		case 0x09:  //6dB_800mV
			lt8911b_i2c_write(0x18, 0x01);
			lt8911b_i2c_write(0x19, 0x00);
			        //lt8911b_i2c_write(0x10,0x00);
			lt8911b_i2c_write(0x11, 0x58);
			ret = 0x11;
			break;

		case 0x0a:
			break;

		case 0x0b:
			ret = 0x17;
			break;

		case 0x0c:  //9.5dB_400mV
			lt8911b_i2c_write(0x18, 0x00);
			lt8911b_i2c_write(0x19, 0xc0);
			        //lt8911b_i2c_write(0x10,0x00);
			lt8911b_i2c_write(0x11, 0x78);
			ret = 0x78;
			break;

		case 0x0d:
			break;

		case 0x0e:
			ret = 0x3a;
			break;

		case 0x0f:
			break;

		default:  break;
	}

	DpcdWrite(0x0103, ret);

#ifdef _2_Lane_
	ret = 0x00;
	swing_req = DPCD0206H & 0xf0;   //lane 1
	lt8911b_i2c_write(0xff, 0x81);

	switch(swing_req)
	{
		case 0x00:                  //0dB_400mV
			lt8911b_i2c_write(0x1a, 0x00);
			lt8911b_i2c_write(0x1b, 0xa0);
			lt8911b_i2c_write(0x13, 0x00);
			ret = 0x00;
			break;

		case 0x10:                  //0dB_600mV
			lt8911b_i2c_write(0x1a, 0x00);
			lt8911b_i2c_write(0x1b, 0xd0);
			                        //lt8911b_i2c_write(0x12,0x00);
			lt8911b_i2c_write(0x13, 0x00);
			ret = 0x01;
			break;

		case 0x20:                  //0dB_800mV
			lt8911b_i2c_write(0x1a, 0x01);
			lt8911b_i2c_write(0x1b, 0x20);
			                        //lt8911b_i2c_write(0x12,0x00);
			lt8911b_i2c_write(0x13, 0x00);
			ret = 0x02;
			break;

		case 0x30:                  //0dB_1200mV(max 1000mV)
			lt8911b_i2c_write(0x1a, 0x01);
			lt8911b_i2c_write(0x1b, 0xa0);
			                        //lt8911b_i2c_write(0x12,0x00);
			lt8911b_i2c_write(0x13, 0x00);
			ret = 0x07;
			break;

		case 0x40:                  //3.5dB_400mV
			lt8911b_i2c_write(0x1a, 0x00);
			lt8911b_i2c_write(0x1b, 0x98);
			                        //lt8911b_i2c_write(0x12,0x00);
			lt8911b_i2c_write(0x13, 0x28);
			ret = 0x08;
			break;

		case 0x50:                  //3.5dB_600mV
			lt8911b_i2c_write(0x1a, 0x00);
			lt8911b_i2c_write(0x1b, 0xf0);
			                        //lt8911b_i2c_write(0x12,0x00);
			lt8911b_i2c_write(0x13, 0x38);
			ret = 0x09;
			break;

		case 0x60:                  //3.5dB_800mV
			lt8911b_i2c_write(0x1a, 0x01);
			lt8911b_i2c_write(0x1b, 0xa0);
			                        //lt8911b_i2c_write(0x12,0x00);
			lt8911b_i2c_write(0x13, 0x70);
			ret = 0x0a;
			break;

		case 0x70:
			break;

		case 0x80:  //6dB_400mV
			lt8911b_i2c_write(0x1a, 0x00);
			lt8911b_i2c_write(0x1b, 0xa0);
			        //lt8911b_i2c_write(0x12,0x00);
			lt8911b_i2c_write(0x13, 0x44);
			ret = 0x12;
			break;

		case 0x90:  //6dB_800mV
			lt8911b_i2c_write(0x1a, 0x01);
			lt8911b_i2c_write(0x1b, 0x00);
			        //lt8911b_i2c_write(0x12,0x00);
			lt8911b_i2c_write(0x13, 0x58);
			ret = 0x13;
			break;

		case 0xa0:
			break;

		case 0xb0:
			ret = 0x17;
			break;

		case 0xc0:  //9.5dB_400mV
			lt8911b_i2c_write(0x1a, 0x00);
			lt8911b_i2c_write(0x1b, 0xc0);
			        //lt8911b_i2c_write(0x12,0x00);
			lt8911b_i2c_write(0x13, 0x78);
			ret = 0x78;
			break;

		case 0xd0:
			break;

		case 0xe0:
			ret = 0x3a;
			break;

		case 0xf0:
			break;

		default:  break;
	}

	DpcdWrite(0x0104, ret);

#endif
}


void LT8911_AUX_Training(void)
{
	unsigned char swing_adj_cnt = 0x00;
	DPCD0202H  = 0x00;
	DPCD0000H  = DpcdRead(0x0000);
	DPCD0200H  = DpcdRead(0x0200);
	DPCD0201H  = DpcdRead(0x0201);
	DPCD0202H  = DpcdRead(0x0202);
	DPCD0203H  = DpcdRead(0x0203);
	DPCD0204H  = DpcdRead(0x0204);
	DPCD0205H  = DpcdRead(0x0205);
	DPCD0000H  = DpcdRead(0x0000);
	DPCD0001H  = DpcdRead(0x0001);
	DPCD0002H  = DpcdRead(0x0002);
	DPCD0003H  = DpcdRead(0x0003);
	DPCD0004H  = DpcdRead(0x0004);
	DPCD0005H  = DpcdRead(0x0005);
	DPCD0006H  = DpcdRead(0x0006);
	DPCD0007H  = DpcdRead(0x0007);
	DPCD0008H  = DpcdRead(0x0008);
	DPCD0009H  = DpcdRead(0x0009);
	DPCD000aH  = DpcdRead(0x000a);
	DPCD000bH  = DpcdRead(0x000b);

	lt8911b_i2c_write(0xff, 0x80);   //register bank
	lt8911b_i2c_write(0x62, 0x3f);   //Reset dp video

#ifdef _2_Lane_
	lt8911b_i2c_write(0x03, 0x42);   //41-1lane,42-2lane,44-4lane
#endif

	lt8911b_i2c_write(0x65, 0xf1);
	mdelay(5);
	lt8911b_i2c_write(0x65, 0xf3);

	lt8911b_i2c_write(0x04, 0x14);

	lt8911b_i2c_write(0xff, 0x84); //register bank
	//  lt8911b_i2c_write(0x14,0x01);
	lt8911b_i2c_write(0x14, 0x81);
	lt8911b_i2c_write(0x14, 0x82);

	DpcdWrite(0x0600, 0x01);

	if(DpcdRead(0x0600) != 0x01)
	{
		DpcdWrite(0x0600, 0x01);
	}

	DpcdWrite(0x0100, 0x0a);

#ifdef _2_Lane_
	DpcdWrite(0x0101, 0x82);  // 2 lane
#endif

	DpcdWrite(0x010a, 0x00);
	//  DpcdWrite(0x0107,0x00);
	//  DpcdWrite(0x0108,0x01);

	if(DpcdRead(0x0100) != 0x0a)
	{
		DpcdWrite(0x0100, 0x0a);
	}

#ifdef _2_Lane_
	if(DpcdRead(0x0101) != 0x82)    // 2 Lane
	{
		DpcdWrite(0x0101, 0x82);
	}
#endif

	if(DpcdRead(0x010a) != 0x00)
	{
		DpcdWrite(0x010a, 0x00);
	}

	//  DpcdWrite(0x0102,0x00);
	DpcdWrite(0x0102, 0x01); // sent TPS1
	DpcdWrite(0x0103, 0x00);

#ifdef _2_Lane_
	DpcdWrite(0x0104, 0x00);
#endif

	if(DpcdRead(0x0102) != 0x01)
	{
		DpcdWrite(0x0102, 0x01);
	}

	mdelay(16);
	DPCD0204H  = DpcdRead(0x0204);
	DPCD0202H  = DpcdRead(0x0202);
	printf("LHT DPCD0204H[%02x] DPCD0202H[%02x]\n",DPCD0204H,DPCD0202H);
	swing_adj_cnt = 0x05;

#ifdef _2_Lane_
	DPCD0202H = DPCD0202H & 0x11;                                       // 2 Lane 0x11 ; 1 Lane 0x01
	while(((DPCD0202H & 0x11) != 0x11) && (swing_adj_cnt > 0))  // 1080P 0x11 ; 1366 0x01
#endif
	{
		DPCD0206H = DpcdRead(0x0206);
		adj_swing();
		swing_adj_cnt--;
		mdelay(1);
		DPCD0202H = DpcdRead(0x0202);
#ifdef _2_Lane_
		DPCD0202H = DPCD0202H & 0x11;   // 2 Lane 0x11 ; 1 Lane 0x01
#endif

	}

	//  lt8911b_i2c_write(0xff,0x82);   //for debug
	//  lt8911b_i2c_write(0x1b,DPCD0202H);

#ifdef _2_Lane_
	if(DPCD0202H == 0x11)                 // 2 Lane 0x11 ; 1 Lane 0x01
#endif
	{
		lt8911b_i2c_write(0xff, 0x80);   //register bank
		lt8911b_i2c_write(0x04, 0x18);

		lt8911b_i2c_write(0xff, 0x84);   //register bank
		//  lt8911b_i2c_write(0x14,0x04);
		lt8911b_i2c_write(0x14, 0x84);
		lt8911b_i2c_write(0x14, 0x88);   //0x88

		DpcdWrite(0x0102, 0x02);          // sent TPS2
		if(DpcdRead(0x0102) != 0x02)
		{
			DpcdWrite(0x0102, 0x02);
		}

		mdelay(16);
		DPCD0204H  = DpcdRead(0x0204);
		DPCD0202H  = DpcdRead(0x0202);

		swing_adj_cnt = 0x05;

#ifdef _2_Lane_
		while(((DPCD0202H & 0x77) != 0x77) && (swing_adj_cnt > 0))  // 2 Lane 0x77 ; 1 Lane 0x07
#endif
		{
			DPCD0206H = DpcdRead(0x0206);
			lt8911b_i2c_write(0xff, 0x84);                               //register bank
			lt8911b_i2c_write(0x14, 0x08);
			lt8911b_i2c_write(0x14, 0x88);
			adj_swing();
			swing_adj_cnt--;
			mdelay(1);
			DPCD0202H  = DpcdRead(0x0202);
			DPCD0204H  = DpcdRead(0x0204);
		}
	}

	//  lt8911b_i2c_write(0xff,0x82);//register bank
	//  lt8911b_i2c_write(0x1c,DPCD0202H);

	lt8911b_i2c_write(0xff, 0x80);       //register bank
	lt8911b_i2c_write(0x04, 0x10);

	lt8911b_i2c_write(0xff, 0x84);       //register bank
	lt8911b_i2c_write(0x14, 0x80);
	lt8911b_i2c_write(0x14, 0xc0);

	lt8911b_i2c_write(0xff, 0x88);       //register bank

	if(lt8911b_i2c_read(0x24) != 0xc0)
	{
		lt8911b_i2c_write(0xff, 0x80);   //register bank
		lt8911b_i2c_write(0x62, 0x3f);
		lt8911b_i2c_write(0x62, 0xbf);
	}

	lt8911b_i2c_write(0xff, 0x80);       //register bank
	lt8911b_i2c_write(0x62, 0xbf);

	lt8911b_i2c_write(0xff, 0x80);       //register bank
	lt8911b_i2c_write(0x65, 0xf1);
	mdelay(5);
	lt8911b_i2c_write(0x65, 0xf3);

	DpcdWrite(0x0102, 0x00);              // sent data

	if(DpcdRead(0x0102) != 0x00)
	{
		DpcdWrite(0x0102, 0x00);
	}

	if(DpcdRead(0x0600) != 0x01)
	{
		DpcdWrite(0x0600, 0x01); 
	}

	if(DpcdRead(0x010a) != 0x00)
	{
		DpcdWrite(0x010a, 0x00); 
	}

	DPCD0202H = DpcdRead(0x0202);
	printf("LHT final DPCD0202H[%02x]!!!\n",DPCD0202H);
}


void reset_lt8911(void)
{
	gpio_tlmm_config(61, 0, GPIO_OUTPUT, GPIO_NO_PULL,GPIO_8MA, GPIO_DISABLE);
	mdelay(150);
	
	gpio_set_dir(61,2);
	mdelay(150);//100
	gpio_set_dir(61,0);
	mdelay(150);//100
	gpio_set_dir(61,2);
	mdelay(300);
}

void lt8911_reg_init()
{
	unsigned char val1,val2;
	
	reset_lt8911(); // 刷寄存器之前，先Reset LT8911/B ,用GPIO 先拉低LT8911B的复位脚 100ms左右，再拉高，保持100ms


	lt8911b_i2c_write(0xff, 0x80);
	val1=lt8911b_i2c_read(0x00);
	val2=lt8911b_i2c_read(0x01);
	printf("lht version3 [%02x][%02x]\n",val1,val2);	
	
	lt8911b_i2c_write(0xff, 0x81);               //register bank
	lt8911b_i2c_write(0x00, 0x04);

	lt8911b_i2c_write(0xff, 0x80);               //register bank
	lt8911b_i2c_write(0x7a, 0x07);
	lt8911b_i2c_write(0x71, 0x36);
	lt8911b_i2c_write(0x72, 0x00);
	lt8911b_i2c_write(0x73, 0x00);

	lt8911b_i2c_write(0x63, 0x7f);
	lt8911b_i2c_write(0x63, 0xff);

	///////////txpll_analog///////
	lt8911b_i2c_write(0xff, 0x81);   //register bank
	lt8911b_i2c_write(0x0e, 0x37);
	lt8911b_i2c_write(0x01, 0x18);
	lt8911b_i2c_write(0x02, 0x42);
	lt8911b_i2c_write(0x04, 0x00);
	lt8911b_i2c_write(0x04, 0x01);

	lt8911b_i2c_write(0xff, 0x80);   //register bank
	lt8911b_i2c_write(0x61, 0x7f);
	lt8911b_i2c_write(0x61, 0xff);

	lt8911b_i2c_write(0xff, 0x81);   //register bank
	lt8911b_i2c_write(0x05, 0x13);

	//////////txpll_digtal////////
	lt8911b_i2c_write(0xff, 0x80);   //register bank
	lt8911b_i2c_write(0x74, 0x41);
	lt8911b_i2c_write(0x75, 0x03);
	lt8911b_i2c_write(0x76, 0x0a);
	lt8911b_i2c_write(0x78, 0x0a);

	//-------------------------------------------//
	lt8911b_i2c_write(0xff, 0x81);   //register bank
	lt8911b_i2c_write(0x0e, 0x37);

	// 2 Lane eDP Output
	lt8911b_i2c_write(0x22, 0x33);   // 关闭 LANE2 / LANE3 SWING的电流开关
	lt8911b_i2c_write(0x23, 0x3c);   // 关闭 LANE2 / LANE3 pre-emphase的电流开关
	lt8911b_i2c_write(0x25, 0x08);

	lt8911b_i2c_write(0x18, Swing_Level_0_H);
	lt8911b_i2c_write(0x19, Swing_Level_0_L);

	lt8911b_i2c_write(0x1a, Swing_Level_0_H);
	lt8911b_i2c_write(0x1b, Swing_Level_0_L);

	//---------------------------------- //

#ifdef _1080P_eDP_Panel_
	lt8911b_i2c_write(0xff, 0x90);                                                       //register bank
	lt8911b_i2c_write(0x4a, 0x33);                                                       // 148.5MHz
	lt8911b_i2c_write(0x4b, 0x33);
	lt8911b_i2c_write(0x4c, 0xd3);
	lt8911b_i2c_write(0x4d, 0x10);
#endif

	lt8911b_i2c_write(0xff, 0x81);                                                       //register bank
	lt8911b_i2c_write(0x09, 0x01);
	lt8911b_i2c_write(0x0b, 0x0b);
	lt8911b_i2c_write(0x08, 0x13);

	lt8911b_i2c_write(0xff, 0x80);                                                       //register bank
	lt8911b_i2c_write(0x63, 0x7f);
	lt8911b_i2c_write(0x63, 0xff);

	//-----------------Main Link---------------------//

	lt8911b_i2c_write(0xff, 0x88);                                                       //register bank
	lt8911b_i2c_write(0x00, 0x6a);
	lt8911b_i2c_write(0x04, 0xff);

	lt8911b_i2c_write(0x05, (unsigned char)(MIPI_Timing[H_tol] / 256));                           //RG_HTOTAL[15:0]
	lt8911b_i2c_write(0x06, (unsigned char)(MIPI_Timing[H_tol] % 256));                           //RG_HTOTAL[7:0]
	lt8911b_i2c_write(0x07, (unsigned char)((MIPI_Timing[H_bp] + MIPI_Timing[H_sync]) / 256));  //RG_HSTART [15:8]
	lt8911b_i2c_write(0x08, (unsigned char)((MIPI_Timing[H_bp] + MIPI_Timing[H_sync]) % 256));  //RG_HSTART[7:0]=110
#ifdef _Test_Pattern_
	lt8911b_i2c_write(0x09, (unsigned char)(MIPI_Timing[H_sync] / 256));//[7]RG_HSPOL;[6:0]RG_HSYNC_WIDTH[14:8]  0x80-->0x00
	printf("lht _Test_Pattern_ 0x09 [%02x]\n",lt8911b_i2c_read(0x09));
	lt8911b_i2c_write(0x0a, (unsigned char)(MIPI_Timing[H_sync] % 256));//RG_HSYNC_WIDTH[7:0]=60
	printf("lht _Test_Pattern_ 0x0a [%02x]\n",lt8911b_i2c_read(0x0a));
#else
	lt8911b_i2c_write(0x09, 0x00);                                                       //[7]RG_HSPOL;[6:0]RG_HSYNC_WIDTH[14:8]	 0x80-->0x00
	lt8911b_i2c_write(0x0a, 0x00);                                                       //RG_HSYNC_WIDTH[7:0]=60
#endif
	lt8911b_i2c_write(0x0b, (unsigned char)(MIPI_Timing[H_act] / 256));                           //RG_HWIDTH[15:8]
	lt8911b_i2c_write(0x0c, (unsigned char)(MIPI_Timing[H_act] % 256));                           //RG_HWIDTH[7:0]
	lt8911b_i2c_write(0x0d, (unsigned char)(MIPI_Timing[V_tol] / 256));                           //RG_VTOTAL [15:8]
	lt8911b_i2c_write(0x0e, (unsigned char)(MIPI_Timing[V_tol] % 256));                           //RG_VTOTAL[7:0]

	lt8911b_i2c_write(0x0f, 0x00);                                                       //RG_TOP_VTOTAL[15:8] //fiexd
	lt8911b_i2c_write(0x10, 0x00);                                                       //RG_TOP_VTOTAL[7:0]  //fixed

	lt8911b_i2c_write(0x11, (unsigned char)((MIPI_Timing[V_bp] + MIPI_Timing[V_sync]) / 256));  //RG_VSTART[15:8]
	lt8911b_i2c_write(0x12, (unsigned char)((MIPI_Timing[V_bp] + MIPI_Timing[V_sync]) % 256));  //RG_VSTART[7:0]
#ifdef _Test_Pattern_
	lt8911b_i2c_write(0x13, (unsigned char)(MIPI_Timing[V_sync] / 256));                          //RG_VSPOL;RG_VSYNC_WIDTH[14:8]  0x80-->0x00
	lt8911b_i2c_write(0x14, (unsigned char)(MIPI_Timing[V_sync] % 256));                          //RG_VSYNC_WIDTH[7:0]
#else
	lt8911b_i2c_write(0x13, 0x00);                                                       //RG_VSPOL;RG_VSYNC_WIDTH[14:8]	 0x80-->0x00
	lt8911b_i2c_write(0x14, 0x00);                                                       //RG_VSYNC_WIDTH[7:0]
#endif
	lt8911b_i2c_write(0x15, (unsigned char)(MIPI_Timing[V_act] / 256));                           //RG_VHEIGTH[15:8]
	lt8911b_i2c_write(0x16, (unsigned char)(MIPI_Timing[V_act] % 256));                           //RG_VHEIGTH[7:0]

#ifdef _8_Bit_ColorDepth_
	lt8911b_i2c_write(0x17, 0x08);                                                       // LVDS Color Depth:   6 bit: 0x00 ;   8 bit: 0x08
	lt8911b_i2c_write(0x18, 0x20);                                                       // LVDS Color Depth:   6 bit: 0x00 ;   8 bit: 0x20
#endif

	lt8911b_i2c_write(0x19, 0x00);
	lt8911b_i2c_write(0x1a, 0x80);
	lt8911b_i2c_write(0x1e, 0x30);
	lt8911b_i2c_write(0x21, 0x00);
#ifdef _Test_Pattern_
	lt8911b_i2c_write(0x2c, 0xdf);
#else
	lt8911b_i2c_write(0x2c, 0xd0);
#endif
	lt8911b_i2c_write(0x2d, 0x00);

	lt8911b_i2c_write(0x4b, 0xfe);

	lt8911b_i2c_write(0x2e, (unsigned char)((MIPI_Timing[V_bp] + MIPI_Timing[V_sync]) % 256));                      //RG_GCM_DE_TOP[6:0]
	lt8911b_i2c_write(0x2f, (unsigned char)((MIPI_Timing[H_bp] + MIPI_Timing[H_sync]) / 256));                      //RG_GCM_DE_DLY[11:8]
	lt8911b_i2c_write(0x30, (unsigned char)((MIPI_Timing[H_bp] + MIPI_Timing[H_sync]) % 256));                      //RG_GCM_DE_DLY[7:0]
	lt8911b_i2c_write(0x31, (unsigned char)(MIPI_Timing[H_act] / 256));                                               //RG_GCM_DE_CNT[11:8]
	lt8911b_i2c_write(0x32, (unsigned char)(MIPI_Timing[H_act] % 256));                                               //RG_GCM_DE_CNT[7:0]
	lt8911b_i2c_write(0x33, (unsigned char)(MIPI_Timing[V_act] / 256));                                               //RG_GCM_DE_LIN[10:8]
	lt8911b_i2c_write(0x34, (unsigned char)(MIPI_Timing[V_act] % 256));                                               //RG_GCM_DE_LIN[7:0]
	lt8911b_i2c_write(0x35, (unsigned char)(MIPI_Timing[H_tol] / 256));                                               //RG_GCM_HTOTAL[11:8]
	lt8911b_i2c_write(0x36, (unsigned char)(MIPI_Timing[H_tol] % 256));                                               //RG_GCM_HTOTAL[7:0]

#ifdef _Test_Pattern_

	lt8911b_i2c_write(0x37, 0x18 + (unsigned char)(MIPI_Timing[V_tol] / 256));                                        //1c:pre-pattern,0c:mipi pattern;RG_GCM_VTOTAL[10:8]
#else

	lt8911b_i2c_write(0x37, 0x18 + (unsigned char)(MIPI_Timing[V_tol] / 256));                                        //1c:pre-pattern,0c:mipi pattern;RG_GCM_VTOTAL[10:8]
#endif

	lt8911b_i2c_write(0x38, (unsigned char)(MIPI_Timing[V_tol] % 256));                                               //RG_GCM_VTOTAL[7:0]
	lt8911b_i2c_write(0x39, 0x00);                                                                           //reseve
	lt8911b_i2c_write(0x3a, ((unsigned char)(MIPI_Timing[V_sync] % 256)) * 4 + (unsigned char)(MIPI_Timing[H_sync] / 256));  //RG_GCM_VWIDTH[5:0];RG_GCM_HWIDTH[9:8]
	lt8911b_i2c_write(0x3b, (unsigned char)(MIPI_Timing[H_sync] % 256));                                              //RG_GCM_HWIDTH[7:0]

	////////////////////Nvid//////////////
	lt8911b_i2c_write(0xff, 0x8c);                                                                           //register bank
	lt8911b_i2c_write(0x00, 0x00);
	lt8911b_i2c_write(0x01, 0x80);
	lt8911b_i2c_write(0x02, 0x00);

	//-----------------Training-----------------------------//

	LT8911_AUX_Training();
#ifdef _2_Lane_
	if(DPCD0202H != 0x77)
#endif
	LT8911_AUX_Training();

	//  lt8911b_i2c_write(0xff,0x88);//register bank
	//  lt8911b_i2c_write(0x1e,0x30);
	//  lt8911b_i2c_write(0x4b,0xfe);

	//-----------------------------------------------//

	lt8911b_i2c_write(0xff, 0x81);   //register bank
	lt8911b_i2c_write(0x32, 0x40);   // 0x40
	lt8911b_i2c_write(0x27, 0x80);
	lt8911b_i2c_write(0x28, 0xa4);
	lt8911b_i2c_write(0x29, 0x66);   // 0xd2
	lt8911b_i2c_write(0x2a, 0x04);
	lt8911b_i2c_write(0x2b, 0x7f);   // 0x7e
	lt8911b_i2c_write(0x2c, 0x02);
	lt8911b_i2c_write(0x2d, 0x7c);   // 0x02
	lt8911b_i2c_write(0x2e, 0x00);   // 0xaa
	lt8911b_i2c_write(0x2f, 0x02);
	lt8911b_i2c_write(0x30, 0xaa);
	lt8911b_i2c_write(0x31, 0x4b);
	lt8911b_i2c_write(0x32, 0x43);   // 0x43
	lt8911b_i2c_write(0x33, 0x20);   // 0x20
	lt8911b_i2c_write(0x34, 0x01);   // MIPI Port B power down
	lt8911b_i2c_write(0x35, 0x80);
	lt8911b_i2c_write(0x36, 0xa4);
	lt8911b_i2c_write(0x37, 0xd2);
	lt8911b_i2c_write(0x38, 0x00);
	lt8911b_i2c_write(0x39, 0x36);
	lt8911b_i2c_write(0x3a, 0x00);

	//--------------------------------------------//

	lt8911b_i2c_write(0xff, 0x90);               //register bank
	lt8911b_i2c_write(0x01, 0x01);
	lt8911b_i2c_write(0x02, 0x08);               // 0x04
	lt8911b_i2c_write(0x03, 0x04);
	lt8911b_i2c_write(0x04, 0xc8);
	lt8911b_i2c_write(0x05, 0x00);
	lt8911b_i2c_write(0x06, 0x0b);
	lt8911b_i2c_write(0x0b, _MIPI_Lane_ % 4);    // 00:4 Lane;01:1 Lane;02:2 Lane;03:3 Lane
	lt8911b_i2c_write(0x0c, 0x00);               // 3210
	lt8911b_i2c_write(0x10, 0x03);
	lt8911b_i2c_write(0x11, 0x03);

	lt8911b_i2c_write(0x12, (unsigned char)(MIPI_Timing[H_sync] % 256));
	lt8911b_i2c_write(0x13, (unsigned char)(MIPI_Timing[V_sync] % 256));
	lt8911b_i2c_write(0x14, (unsigned char)(MIPI_Timing[H_act] % 256));
	lt8911b_i2c_write(0x15, (unsigned char)(MIPI_Timing[H_act] / 256));
	lt8911b_i2c_write(0x16, (unsigned char)(MIPI_Timing[H_act] % 256));
	lt8911b_i2c_write(0x17, (unsigned char)(MIPI_Timing[H_act] / 256));

	lt8911b_i2c_write(0x18, 0x00);
	lt8911b_i2c_write(0x19, 0x01);
	lt8911b_i2c_write(0x1a, 0x17);
	lt8911b_i2c_write(0x2b, 0x0b);
	lt8911b_i2c_write(0x2c, 0x0c);

	lt8911b_i2c_write(0x31, (unsigned char)(MIPI_Timing[H_tol] % 256));
	lt8911b_i2c_write(0x32, (unsigned char)(MIPI_Timing[H_tol] / 256));
	lt8911b_i2c_write(0x33, (unsigned char)(MIPI_Timing[V_tol] % 256));
	lt8911b_i2c_write(0x34, (unsigned char)(MIPI_Timing[V_tol] / 256));
	lt8911b_i2c_write(0x35, (unsigned char)(MIPI_Timing[V_bp] % 256));
	lt8911b_i2c_write(0x36, (unsigned char)(MIPI_Timing[V_bp] / 256));
	lt8911b_i2c_write(0x37, (unsigned char)((MIPI_Timing[V_tol] - MIPI_Timing[V_act] - MIPI_Timing[V_bp] - MIPI_Timing[V_sync]) % 256));
	lt8911b_i2c_write(0x38, (unsigned char)((MIPI_Timing[V_tol] - MIPI_Timing[V_act] - MIPI_Timing[V_bp] - MIPI_Timing[V_sync]) / 256));
	lt8911b_i2c_write(0x39, (unsigned char)(MIPI_Timing[H_bp] % 256));
	lt8911b_i2c_write(0x3a, (unsigned char)(MIPI_Timing[H_bp] / 256));
	lt8911b_i2c_write(0x3b, (unsigned char)((MIPI_Timing[H_tol] - MIPI_Timing[H_act] - MIPI_Timing[H_bp] - MIPI_Timing[H_sync]) % 256));
	lt8911b_i2c_write(0x3c, (unsigned char)((MIPI_Timing[H_tol] - MIPI_Timing[H_act] - MIPI_Timing[H_bp] - MIPI_Timing[H_sync]) / 256));

	lt8911b_i2c_write(0x1b, 0x5e);
	lt8911b_i2c_write(0x1c, 0x01);
	lt8911b_i2c_write(0x1d, 0x2c);
	lt8911b_i2c_write(0x1e, 0x01);
	lt8911b_i2c_write(0x1f, 0xfa);
	lt8911b_i2c_write(0x20, 0x00);
	lt8911b_i2c_write(0x21, 0xc8);
	lt8911b_i2c_write(0x22, 0x00);
	lt8911b_i2c_write(0x23, 0x5e);
	lt8911b_i2c_write(0x24, 0x01);
	lt8911b_i2c_write(0x25, 0x2c);
	lt8911b_i2c_write(0x26, 0x01);
	lt8911b_i2c_write(0x27, 0xfa);
	lt8911b_i2c_write(0x28, 0x00);
	lt8911b_i2c_write(0x29, 0xc8);
	lt8911b_i2c_write(0x2a, 0x00);
	lt8911b_i2c_write(0x3d, 0x64);  
	lt8911b_i2c_write(0x3f, 0x00);  

	lt8911b_i2c_write(0x40, 0x04);
	lt8911b_i2c_write(0x41, 0x00);
	lt8911b_i2c_write(0x42, 0x59);
	lt8911b_i2c_write(0x43, 0x00);
	lt8911b_i2c_write(0x44, 0xf2);
	lt8911b_i2c_write(0x45, 0x06);
	lt8911b_i2c_write(0x46, 0x00);
	lt8911b_i2c_write(0x47, 0x72);
	lt8911b_i2c_write(0x48, 0x45);
	lt8911b_i2c_write(0x49, 0x00);

	lt8911b_i2c_write(0x60, 0x08);
	lt8911b_i2c_write(0x61, 0x00);
	lt8911b_i2c_write(0x62, 0xb2);
	lt8911b_i2c_write(0x63, 0x00);
	lt8911b_i2c_write(0x64, 0xe4);
	lt8911b_i2c_write(0x65, 0x0d);
	lt8911b_i2c_write(0x66, 0x00);
	lt8911b_i2c_write(0x67, 0xe4);
	lt8911b_i2c_write(0x68, 0x8a);
	lt8911b_i2c_write(0x69, 0x00);
	lt8911b_i2c_write(0x6a, 0x0b);  
	lt8911b_i2c_write(0x1a, 0x4f);  
	lt8911b_i2c_write(0x6b, 0x04);  

#ifdef _Test_Pattern_
	// 前面已经设置过了，这里不需设置。
	//  lt8911b_i2c_write(0x4d,0x10);
#else
	lt8911b_i2c_write(0xff, 0x90);   //register bank
	lt8911b_i2c_write(0x4d, 0x00);
#endif

	//---------------------------------------//


#ifndef _Test_Pattern_

	lt8911b_i2c_write(0xff, 0x80);   //register bank
	lt8911b_i2c_write(0x62, 0x3f);

	/**********************
	   52 ff 90 00
	   52 75 01 ff //bit[23:16]
	   52 76 01 ff //bit[15:8]
	   52 77 01 ff //bit[7:0]
	 ***********************/

	//*
	// 连续读5次0x9076寄存器，如果值相差小于3，说明DDS已经调稳.
	while(Count < 4)
	{
		Count++;
		lt8911b_i2c_write(0xff, 0x90);
		mdelay(100);
		Pre_Temp = lt8911b_i2c_read(0x76);
		mdelay(100);
		Cur_Temp = lt8911b_i2c_read(0x76);

		D_value = (Cur_Temp >= Pre_Temp) ? (Cur_Temp - Pre_Temp) : (Pre_Temp - Cur_Temp);

		// 连续读两次0x9076寄存器，如果值相差大于3，复位一下0x8063/0x8060寄存器。
		while(D_value >= 0x03)
		{
			Count = 0x00;
			lt8911b_i2c_write(0xff, 0x80); //register bank
			lt8911b_i2c_write(0x63, 0x3f);
			lt8911b_i2c_write(0x63, 0xbf);

			lt8911b_i2c_write(0x60, 0xde);
			lt8911b_i2c_write(0x60, 0xff);

			lt8911b_i2c_write(0xff, 0x81); //register bank
			lt8911b_i2c_write(0x32, 0x40);
			lt8911b_i2c_write(0x32, 0x43);

			mdelay(300);

			lt8911b_i2c_write(0xff, 0x90); //register bank

			Pre_Temp = lt8911b_i2c_read(0x76);
			mdelay(100);
			Cur_Temp = lt8911b_i2c_read(0x76);

			D_value = (Cur_Temp >= Pre_Temp) ? (Cur_Temp - Pre_Temp) : (Pre_Temp - Cur_Temp);
		}
	}
	//*/

	lt8911b_i2c_write(0xff, 0x80); //register bank
	lt8911b_i2c_write(0x63, 0x3f);
	lt8911b_i2c_write(0x63, 0xbf);

	lt8911b_i2c_write(0x60, 0xde);
	lt8911b_i2c_write(0x60, 0xff);

	mdelay(200);

	lt8911b_i2c_write(0xff, 0x88);
	lt8911b_i2c_write(0x37, 0x08 + (unsigned char)(MIPI_Timing[V_tol] / 256));

	mdelay(200);

	lt8911b_i2c_write(0xff, 0x80);
	lt8911b_i2c_write(0x62, 0xbf);
	//*/
#endif
	//------------------------------------------//
	//  For test
	//  lt8911b_i2c_write(0xff,0x90);//register bank
	//  lt8911b_i2c_write(0x07,0xc0);

	//  lt8911b_i2c_write(0xff,0x80);//register bank
	//  lt8911b_i2c_write(0x94,0x00);
	//  lt8911b_i2c_write(0x95,0x00);

	//  lt8911b_i2c_write(0xff,0x81);//register bank
	//  lt8911b_i2c_write(0x3f,0x02);
	//  lt8911b_i2c_write(0x3e,0xff);
	//  lt8911b_i2c_write(0x3d,0x03);
	//  lt8911b_i2c_write(0x2b,0x7f);
	//----------------------------------------//

	lt8911b_i2c_write(0xff, 0x00); //register bank
}
//---------------------------------------------//

// MIPI输入点 eDP屏的 LT8911B 寄存器设置：

//**************************************************//
// 确定要在给LT8911B 提供MIPI信号之后，再初始化LT8911B寄存器。
//*************************************************** //

void LT8911B_Initial(void)
{	
	//unsigned char count;

	//count=0;
	LK_lt8911_i2c_init();//初始化i2c

//L1: 
lt8911_reg_init();
	
	printf("---------LHT final DPCD0202H[%02x]----------\n",DPCD0202H);
    /*
	if(DPCD0202H!=0x77)
	{
		count++;
		if(count<=5)
			goto L1;
		else	
		{
			printf("LT8911B init fail,reboot!!!\n");
			reboot_device(0);
		}
	}
	*/
}
/************************************** The End Of File **************************************/
