package com.android.server;

class FileInfoBean {
	private String path;
	private String targetVersion;
	private String baseVersion;
	private boolean isavaliable;
	
	
	public FileInfoBean(String path, String targetVersion, String baseVersion,
			boolean isavaliable) {
		super();
		this.path = path;
		this.targetVersion = targetVersion;
		this.baseVersion = baseVersion;
		this.isavaliable = isavaliable;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getTargetVersion() {
		return targetVersion;
	}
	public void setTargetVersion(String targetVersion) {
		this.targetVersion = targetVersion;
	}
	public String getBaseVersion() {
		return baseVersion;
	}
	public void setBaseVersion(String baseVersion) {
		this.baseVersion = baseVersion;
	}
	public boolean isIsavaliable() {
		return isavaliable;
	}
	public void setIsavaliable(boolean isavaliable) {
		this.isavaliable = isavaliable;
	}
	
}