package com.szzt.ztticaisystem;

import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.Log;

import com.szzt.util.SzztErrorTable;
import com.szzt.util.SzztLogUtils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by yxljl1314 on 2017/12/21.
 *
 * @hide
 */
public class ZtSystemSdkManager {
    public static final String TAG = ZtSystemSdkManager.class.getSimpleName();
    public static final int NETWORK_TYPE_ALL = 0;
    public static final int NETWORK_TYPE_ETHERNET = 1;
    public static final int NETWORK_TYPE_4G = 2;
    public static final int NETWORK_TYPE_WIFI = 3;
    public static final int NETWORK_AVAILABLE = 1;
    public static final int NETWORK_UNAVAILABLE = 0;
    private static ZtSystemSdkManager ztSystemSdkManager = null;
    private IZtSystemServer mIZtSystemServer = null;
    private static final String SERVER_FLAG = "ztsystemservice";
    public static byte[] calculationFileMD5B(String filepath){
        if(TextUtils.isEmpty(filepath)){
            return null;
        }
        try {
            FileInputStream fileInputStream = new FileInputStream(filepath);
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = fileInputStream.read(buffer, 0, 1024)) != -1) {
                md.update(buffer, 0, length);
            }
            return md.digest();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String calculationFileMD5S(String filepath){
        byte[] valueBytes = calculationFileMD5B(filepath);
        if(valueBytes != null){
            return SzztLogUtils.bytesToHexString(valueBytes);
        }
        return null;
    }
    public synchronized static ZtSystemSdkManager getInstance(){
        if(ztSystemSdkManager == null){
            ztSystemSdkManager = new ZtSystemSdkManager();
        }
        return ztSystemSdkManager;
    }
    private IBinder getServiceIBinder(){
        try {
            final Class<?> systemPropertyClass = Class.forName("android.os.ServiceManager");
            Method method = systemPropertyClass.getMethod("getService", String.class);
            return (IBinder) method.invoke(null,SERVER_FLAG);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
    private IBinder.DeathRecipient mDeathRecipient = new IBinder.DeathRecipient() {
        @Override
        public void binderDied() {
            mIZtSystemServer = getService();
        }
    };
    private synchronized IZtSystemServer getService(){
        try {
            Log.e(TAG,"system api,getService");
            IBinder iBinder = getServiceIBinder();
            if(iBinder != null){
                iBinder.linkToDeath(mDeathRecipient,0);
                IZtSystemServer iSzztSystemSdk = IZtSystemServer.Stub.asInterface(iBinder);
                return iSzztSystemSdk;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return null;
    }
    private ZtSystemSdkManager(){
        checkServerNoNull();
    }
    private boolean checkServerNoNull(){
        if(mIZtSystemServer == null){
            mIZtSystemServer = getService();
        }
        if(mIZtSystemServer == null){
            SzztLogUtils.e(TAG,"server is null");
            return false;
        }else{
            return true;
        }
    }
    public boolean isSupportAppManager(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.isSupportAppManager();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
    public int appInstall(String apkPath){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.appInstall(apkPath);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public int appUpdate(String apkPath){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.appUpdate(apkPath);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public int appUninstall(String packageName){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.appUninstall(packageName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public boolean isSupportAppDataManager(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.isSupportAppDataManager();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public void cleanAppData(String appPkgName){
        if(checkServerNoNull()){
            try {
                mIZtSystemServer.cleanAppData(appPkgName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
    public int appInstallAndStart(String apkPath, String clsName){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.appInstallAndStart(apkPath,clsName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public int startApp(String pkgName, String clsName){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.startApp(pkgName,clsName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }

    public int setSystemHistorySaveDir(String dirname){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.setSystemHistorySaveDir(dirname);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }

    public boolean writeOperationLog(String msg){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.writeOperationLog(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean writeUpdateLog(String msg){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.writeUpdateLog(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean writeAppsLog(String msg){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.writeAppsLog(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean writeAuthorityLog(String msg){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.writeAuthorityLog(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }

    public int getLastOperationErrorCode(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.getLastOperationErrorCode();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public String getLastOperationErrorString(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.getLastOperationErrorString();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.toString();
    }

    public int isNetworkAvailable(int type){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.isNetworkAvailable(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public boolean enableNetwork(int type){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.enableNetwork(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean disableNetwork(int type){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.disableNetwork(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public int isNetworkEnable(int type){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.isNetworkEnable(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public boolean openNetwork(int type){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.openNetwork(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean closeNetwork(int type){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.closeNetwork(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public int isNetworkOpen(int type){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.isNetworkOpen(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public int currentNetworkType(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.currentNetworkType();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public boolean setNetworkMode(int type , int mode,Intent parameters){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.setNetworkMode(type,mode,parameters);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public int getNetworkMode(int type){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.getNetworkMode(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public Intent getNetworkParameters(int type,int mode){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.getNetworkParameters(type,mode);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
        return null;
    }

    public Intent getEthernetDhcpNetworkParameters(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.getEthernetDhcpNetworkParameters();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
        return null;
    }

    public int msecFromLastOperation(int opertype){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.msecFromLastOperation(opertype);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public boolean startAdbServer(int port){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.startAdbServer(port);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean stopAdbServer(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.stopAdbServer();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean setSdCardState(boolean isEnable){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.setSdCardState(isEnable);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public int getSdCardState(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.getSdCardState();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public int getFlashCapacity(int flag,String path){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.getFlashCapacity(flag, path);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public void reboot(){
        if(checkServerNoNull()){
            try {
                mIZtSystemServer.reboot();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
    }
    public void poweroff(){
        if(checkServerNoNull()){
            try {
                mIZtSystemServer.poweroff();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
    }
    public boolean autoShutDown(int year,int month,int day,int hour,int min,int sec,int msec){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.autoShutDown(year, month, day, hour, min, sec, msec);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public String screenshot(int index, String fileName){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.screenshot(index, fileName);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
        return null;
    }
    public int clearDevice(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.clearDevice();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }

    public void factoryReset(){
        if(checkServerNoNull()){
            try {
                mIZtSystemServer.factoryReset();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
    }
    public boolean firmwareDownloadFromFile(String upgradeFilePath){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.firmwareDownloadFromFile(upgradeFilePath);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public String lastUpdateResult(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.lastUpdateResult();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
        return null;
    }
    public String getFirmwareInformation(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.getFirmwareInformation();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
        return null;
    }
    public int lock(String passwd){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.lock(passwd);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public int unlock(String passwd){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.unlock(passwd);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }
    public int unlockFailedAttempts(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.unlockFailedAttempts();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }

    public boolean setNtpServer(String server,int port){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.setNtpServer(server, port);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean syncTime(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.syncTime();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean restartNtp(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.restartNtp();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean setTime(int year,int month,int day,int hour,int min,int sec,int msec){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.setTime(year, month, day, hour, min, sec, msec);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }

    public boolean setTimeZt(String pkgName,int year,int month,int day,int hour,int min,int sec,int msec){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.setTimeZt(pkgName,year, month, day, hour, min, sec, msec);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }

    public void setDisplaySizeForZt(int displayId, int width, int height){
        if(checkServerNoNull()){
            try {
                mIZtSystemServer.setDisplaySizeForZt(displayId,width,height);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public String checkSystemUpdate2(String cfgFile){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.checkSystemUpdate2(cfgFile);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
        return null;
    }

    public byte[] readSn(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.readSn();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
        return null;
    }

    public byte[] readProductDate() {
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.readProductDate();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        SzztLogUtils.e(TAG,SzztErrorTable.ErrorTable.NullObject.toString());
        return null;
    }

    public int writeNvData(int item, byte[] data, int len){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.writeNvData(item, data, len);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorCode();
    }

    public boolean IPSecDial(Intent intent){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.IPSecDial(intent);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }

    public boolean ADSLDial(Intent intent){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.ADSLDial(intent);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }

    public boolean disconnectDial(int type) {
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.disconnectDial(type);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return SzztErrorTable.ErrorTable.NullObject.getErrorBooleanCode();
    }
    public boolean adslIsConnect(){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.adslIsConnect();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public String getPkgNameForPid(int pid){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.getPkgNameForPid(pid);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public String getVersionForPid(int pid){
        if(checkServerNoNull()){
            try {
                return mIZtSystemServer.getVersionForPid(pid);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
