import shutil,os.path


home_path=".."
secimage_path="./common/sectools/secimage_output/8917"
tz_path=home_path+"/TZ.BF.4.0.5/trustzone_images/build/ms/bin"
boot_path=home_path+"/BOOT.BF.3.3/boot_images/build/ms/bin"
adsp_path=home_path+"/ADSP.8953.2.8.2/adsp_proc/obj/8937"
modem_path=home_path+"/MPSS.JO.3.0/modem_proc/build/ms/bin"
rpm_path=home_path+"/RPM.BF.2.2/rpm_proc/build/ms/bin"
wcnss_path=home_path+"/CNSS.PR.4.0/wcnss_proc/build/ms/bin"
video_path=home_path+"/VIDEO.VE_ULT.3.1/venus_proc/build/bsp/asic/build/PROD/mbn"
android_path=home_path+"/LA.UM.5.6/LINUX/android/out/target/product/msm8937_64"







shutil.copy((secimage_path+"/sbl1/sbl1.mbn"), (boot_path+"/LAASANAZ/sbl1.mbn"))
shutil.copy((secimage_path+"/prog_emmc_firehose_ddr/prog_emmc_firehose_8917_ddr.mbn"), (boot_path+"/LAADANAZ/prog_emmc_firehose_89317_ddr.mbn"))
shutil.copy((secimage_path+"/prog_emmc_firehose_lite/prog_emmc_firehose_8917_lite.mbn"), (boot_path+"/LAADANAZ/prog_emmc_firehose_8917_lite.mbn"))
shutil.copy((secimage_path+"/validated_emmc_firehose_ddr/validated_emmc_firehose_8917_ddr.mbn"), (boot_path+"/LAADANAZ/validated_emmc_firehose_8917_ddr.mbn"))
shutil.copy((secimage_path+"/validated_emmc_firehose_lite/validated_emmc_firehose_8917_lite.mbn"), (boot_path+"/LAADANAZ/validated_emmc_firehose_8917_lite.mbn"))
shutil.copy((secimage_path+"/qsee/tz.mbn"), (tz_path+"/ZALAANAA/tz.mbn"))
shutil.copy((secimage_path+"/appsbl/emmc_appsboot.mbn"), (android_path+"/emmc_appsboot.mbn"))
shutil.copy((secimage_path+"/mba/mba.mbn"), (modem_path+"/8937.genns.prod/mba.mbn"))
shutil.copy((secimage_path+"/modem/modem.mbn"), (modem_path+"/8937.genns.prod/qdsp6sw.mbn"))
shutil.copy((secimage_path+"/rpm/rpm.mbn"), (rpm_path+"/8917/rpm.mbn"))
shutil.copy((secimage_path+"/wcnss/wcnss.mbn"), (wcnss_path+"/8937/reloc/wcnss.mbn"))
shutil.copy((secimage_path+"/venus/venus.mbn"), (video_path+"/reloc/signed/venus.mbn"))
shutil.copy((secimage_path+"/devcfg/devcfg.mbn"), (tz_path+"/ZALAANAA/devcfg.mbn"))
shutil.copy((secimage_path+"/smplap32/smplap32.mbn"), (tz_path+"/ZALAANAA/smplap32.mbn"))
shutil.copy((secimage_path+"/smplap64/smplap64.mbn"), (tz_path+"/ZALAANAA/smplap64.mbn"))
shutil.copy((secimage_path+"/isdbtmm/isdbtmm.mbn"), (tz_path+"/ZALAANAA/isdbtmm.mbn"))
shutil.copy((secimage_path+"/widevine/widevine.mbn"), (tz_path+"/ZALAANAA/widevine.mbn"))
shutil.copy((secimage_path+"/cmnlib/cmnlib.mbn"), (tz_path+"/ZALAANAA/cmnlib.mbn"))
shutil.copy((secimage_path+"/cmnlib64/cmnlib64.mbn"), (tz_path+"/ZALAANAA/cmnlib64.mbn"))
shutil.copy((secimage_path+"/keymaster/keymaster.mbn"), (tz_path+"/ZALAANAA/keymaster.mbn"))
shutil.copy((secimage_path+"/mdtp/mdtp.mbn"), (tz_path+"/ZALAANAA/mdtp.mbn"))
shutil.copy((secimage_path+"/fingerprint/fingerprint.mbn"), (tz_path+"/ZALAANAA/fingerprint.mbn"))
shutil.copy((secimage_path+"/fingerprint64/fingerprint64.mbn"), (tz_path+"/ZALAANAA/fingerprint64.mbn"))
shutil.copy((secimage_path+"/dhsecapp/dhsecapp.mbn"), (tz_path+"/ZALAANAA/dhsecapp.mbn"))
shutil.copy((secimage_path+"/qmpsecap/qmpsecap.mbn"), (tz_path+"/ZALAANAA/qmpsecap.mbn"))
shutil.copy((secimage_path+"/cppf/cppf.mbn"), (tz_path+"/ZALAANAA/cppf.mbn"))
shutil.copy((secimage_path+"/adsp/adsp.mbn"), (adsp_path+"/signed/adsp.mbn"))















